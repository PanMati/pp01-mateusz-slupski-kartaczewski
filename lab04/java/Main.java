mport java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

class WinnerWasCalled extends Exception {
}

class Log {

	public static void info() {
		System.out.println("");
	}

	public static void info(String message) {
		System.out.println(message);
	}

}

class Dice {
	private int numberOfSides=6;
	public Dice(int number) {
		this.numberOfSides=number;
	}
	public int roll() {
		Random rand = new Random();
		int result = rand.nextInt(numberOfSides) + 1;

		Log.info("Dice roll: " + result);
		return result;
	}
	public int getNumberOfSides() {
		return this.numberOfSides;
	}

}

class Pawn {

	private int position;
	private int id;

	public Pawn(int id) {

		this.position = 0;
		this.id = id;
		Log.info(this.id + " joined the game.");
	}


	public int getId() {
		return this.id;
	}

	public int getPosition() {
		return this.position;
	}
	public int addPosition(int position) {
		return this.position+=position;
	}


}
class Board {

	public Dice dice;
	public Pawn winner;

	private static int maxPosition = 30;
	private ArrayList<Pawn> pawns;
	private int turnsCounter;
	private int maxTurn;

	public void addPawn(Pawn pawn) {
		pawns.add(pawn);
	}

	public Board(int maxPosition) {

		this.pawns = new ArrayList<Pawn>();
		this.dice = null;
		this.winner = null;
		this.turnsCounter = 0;

		this.maxPosition=maxPosition;
	}

	public Board(int maxPosition,int maxTurn) {

		this.maxPosition=maxPosition;
		this.pawns = new ArrayList<Pawn>();
		this.turnsCounter = 0;
		this.maxTurn=maxTurn;
		this.dice = null;
		this.winner = null;

	}



	public void performTurn() throws WinnerWasCalled {
		this.turnsCounter++;
		Log.info();
		Log.info("Turn " + this.turnsCounter);

		for(Pawn pawn : this.pawns) {
			int rollResult = this.dice.roll();
			int position=pawn.getPosition();
			pawn.addPosition(rollResult);
			Log.info(pawn.getId() + " new position: " + pawn.getPosition());

			if(pawn.getPosition() >= Board.maxPosition) {
				this.winner = pawn;
				throw new WinnerWasCalled();
			}
			if(maxTurn == turnsCounter) {
				this.winner = pawn;
				throw new WinnerWasCalled();
			}
		}
	}

}

public class lab04 {

	public static void main(String[] args) {
		Random rand = new Random();



		Scanner scan = new Scanner(System.in);
		int maxPosition=0;
		int numberOfPlayers=0;
		int numberOfSides=6;
		int numberOfTurns=0;


		while(numberOfTurns<0 || maxPosition<=0 || numberOfSides<=3 ) {
			System.out.println("Max position?");
			maxPosition = scan.nextInt();
			System.out.println("How many turns?(0 - if unlimited)");
			numberOfTurns = scan.nextInt();
			System.out.println("What is the number of sides?(>4)");
			numberOfSides = scan.nextInt();
		}

		Board board = new Board(maxPosition,numberOfTurns);
		board.dice = new Dice(numberOfSides);

		while(numberOfPlayers<2 || numberOfPlayers>10) {
			System.out.println("Number of players [2-10]");
			numberOfPlayers = scan.nextInt();
		}
		for (int i=0;i<numberOfPlayers;i++) {
			Pawn pawn = new Pawn(rand.nextInt(300));
			board.addPawn(pawn);
		}

		try {
			while(true) {
				board.performTurn();
			}
		} catch(WinnerWasCalled exception) {
			Log.info();
			Log.info(board.winner.getId() + " won.");
		}
		scan.close();
	}

}