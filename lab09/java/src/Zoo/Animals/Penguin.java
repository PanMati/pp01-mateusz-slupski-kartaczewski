package Zoo.Animals;

import Zoo.Food.Food;
import Zoo.Food.Meat;
import Zoo.Food.Water;

public class Penguin extends Animal {

    public Penguin(String name) {
        super(name);
    }

    @Override
    Food[] getDiet() {
        return new Food[]{new Meat(), new Water()};
    }