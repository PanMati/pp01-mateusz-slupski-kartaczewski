package Zoo.Animals;

import Zoo.Food.Food;
import Zoo.Food.Meat;
import Zoo.Food.Milk;

public class Monkey extends Animal {

    public Monkey(String name) {
        super(name);
    }

    @Override
    Food[] getDiet() {
        return new Food[]{new Meat(), new Milk()};
    }


}