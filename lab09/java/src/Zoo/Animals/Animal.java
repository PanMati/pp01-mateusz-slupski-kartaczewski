package Zoo.Animals;

import Zoo.Food.Food;
import Zoo.Log;

import java.util.Arrays;

abstract public class Animal {

    private String name;
    private boolean hungry = true;

    Animal(String name) {
        this.name = name;
    }

    abstract Food[] getDiet();

    public void feed(Food food) throws Exception {
        boolean canHeItThis = false;
        for(Food foodInArray: getDiet()){
            if(foodInArray.getClass() == food.getClass()) canHeItThis = true;
        }

        if(!canHeItThis) {
            throw new Exception(getName() + " doesn't eat " + food.getName() + ".");
        }

        hungry = true;
        Log.info(getName() + " fed.");
    }

    public String getName() {
        return getSpecies() + " " + name;
    }

    private String getSpecies() {
        return getClass().getSimpleName();
    }

}
