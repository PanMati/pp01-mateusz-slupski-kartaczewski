package Zoo.Animals;

import Zoo.Food.Food;
import Zoo.Food.Meat;
import Zoo.Food.Water;

public class Elephant extends Animal {

    public Elephant(String name) {
        super(name);
    }

    @Override
    Food[] getDiet() {
        return new Food[]{new Meat(), new Water()};
    }

}
