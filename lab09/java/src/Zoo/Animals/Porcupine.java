package Zoo.Animals;

import Zoo.Food.Food;
import Zoo.Food.Meat;
import Zoo.Food.Water;

public class Porcupine extends Animal {

    public Porcupine(String name) {
        super(name);
    }

    @Override
    Food[] getDiet() { return new Food[]{new Meat(), new Water()};}
