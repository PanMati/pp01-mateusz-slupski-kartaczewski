package Zoo.Animals;

import Zoo.Food.Food;
import Zoo.Food.Meat;
import Zoo.Food.Milk;

public class Lion extends Animal {

    public Lion(String name) {
        super(name);
    }

    @Override
    Food[] getDiet() {
        return new Food[]{new Meat(), new Milk()};
    }


}
