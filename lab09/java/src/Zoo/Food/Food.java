package Zoo.Food;

public abstract class Food {
    String name;

    Food(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
