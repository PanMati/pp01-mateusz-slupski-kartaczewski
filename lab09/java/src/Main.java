import Zoo.Animals.*;
import Zoo.Food.Meat;
import Zoo.Food.Milk;
import Zoo.Food.Water;
import Zoo.Zoo;
import Zoo.Log;

public class Main {

    public static void main(String[] args) {

        String[] hours = {"8:00", "9:00", "10:00"};

        Water water = new Water();
        Meat meat = new Meat();
        Milk milk = new Milk();

        Zoo zoo = new Zoo("Zoo Legnica");

            zoo.addAnimal(new Lion("Simba"))
                .addAnimal(new Lion("Mufasa"))
                .addAnimal(new Elephant("Dumbo"))
                    .addAnimal(new Monkey("MrMonkey"))
                    .addAnimal(new Dolphin("Delfineu"))
                    .addAnimal(new Penguin("BamboPingwinando"))
                    .addAnimal(new Porcupine("Niko"))
                .addAnimal(new Tiger("Daisy"));


        Log.info();

        for(String hour: hours){
            if(hour.equals("8:00")){
                Log.info("At " + hour + " we feed with " + water.getName());
                zoo.feedAnimals(water);
            }

            if(hour.equals("9:00")){
                Log.info("At " + hour + " we feed with " + milk.getName());
                zoo.feedAnimals(milk);
            }

            if(hour.equals("10:00")){
                Log.info("At " + hour + " we feed with " + milk.getName());
                zoo.feedAnimals(meat);
            }

            Log.info();
        }
    }
}
